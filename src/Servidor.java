import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author Guthierrez
 */
public class Servidor {
	private ServerSocket servidor;
	private Socket cliente;
	private Integer numeroConexoes;
	private Jogador jogadorUm;
	private Jogador jogadorDois;
	private Thread threadJogador1;
	private Thread threadJogador2;
	private Integer rodada = 1;
	
	public Jogador getJogadorUm() {
		return jogadorUm;
	}

	public void setJogadorUm(Jogador jogadorUm) {
		this.jogadorUm = jogadorUm;
	}

	public Jogador getJogadorDois() {
		return jogadorDois;
	}

	public void setJogadorDois(Jogador jogadorDois) {
		this.jogadorDois = jogadorDois;
	}

	private Servidor(){
		this.numeroConexoes = 0;
	}
	
	public void iniciarServidor(Integer porta){
		try {
			this.servidor = new ServerSocket(porta);
			System.out.println("Servidor aguardando jogadores!!");
			while(true){
				if (this.numeroConexoes == 0){
					cliente = servidor.accept();
					jogadorUm = new Jogador(cliente);
					enviarMensagem(cliente.getOutputStream(), "Bem vindo ao jogo Player 1!! Aguarde um Player 2!!\n\r");
					this.numeroConexoes++;
					System.out.println("Jogador 1 conectado!!");
				} else if (this.numeroConexoes == 1){
					cliente = servidor.accept();
					jogadorDois = new Jogador(cliente);
					enviarMensagem(cliente.getOutputStream(), "Bem vindo ao jogo Player 2!! O jogo iniciara em instantes!!\n\r");
					this.numeroConexoes++;
					System.out.println("Jogador 2 conectado!!");
					iniciarJogo();
				} else {
					cliente = servidor.accept();
					enviarMensagem(cliente.getOutputStream(), "Servidor ocupado, tente novamente mais tarde!\n\r");
					cliente.close();
				}
			}
		} catch (IOException e) {
			System.out.println("Erro ao iniciar servidor ou mandar mensagem!\n\r");
		}
	}
	
	private void iniciarJogo(){
		try {
			System.out.println("Iniciando jogo..");
			enviarMensagem(jogadorUm.getSocket().getOutputStream(), 
					jogadorDois.getSocket().getOutputStream(), 
					"Ambos jogadores prontos!!! Iniciando game!!\n\rQuem fizer 3 pontos vence!!!\n\r");
			threadJogador1 = new ThreadJogo(jogadorUm, this);
			threadJogador1.start();
			threadJogador2 = new ThreadJogo(jogadorDois, this);
			threadJogador2.start();
		} catch (IOException e) {
			System.out.println("Erro ao enviar mensagem!!\n\r");
		}
	}
	
	public void setEscolha(Integer escolha, Jogador jogador){
		switch(escolha){
		case 1:
			jogador.setEscolha("pedra");
			break;
		case 2:
			jogador.setEscolha("papel");
			break;
		case 3:
			jogador.setEscolha("tesoura");
			break;
		case 4:
			jogador.setEscolha("lagarto");
			break;
		case 5:
			jogador.setEscolha("spock");
			break;
		}
		if(jogadorUm.getEscolha() != null && jogadorDois.getEscolha() != null ){
			avaliarVencedorRodada();
		}
	}
	
	public void avaliarVencedorRodada(){
		System.out.println("Iniciando uma nova rodada..");
		rodada++;
		if(jogadorUm.getEscolha() == jogadorDois.getEscolha()){
			try {
				enviarMensagem(jogadorUm.getSocket().getOutputStream(),
						jogadorDois.getSocket().getOutputStream(),
						"Ambos escolheram a mesma opcaoo, empate!!\n\r");
			} catch (IOException e) {
				System.out.println("Erro ao enviar mensagem!!\n\r");
			}
		}else{
			switch(jogadorUm.getEscolha().trim()){
			case "tesoura":
				if(jogadorDois.getEscolha() == "papel" || jogadorDois.getEscolha() == "lagarto"){
					jogadorUm.setPontuacao(jogadorUm.getPontuacao() + 1);
					informarVencedorRodada(jogadorUm.getNome());
				}else{
					jogadorDois.setPontuacao(jogadorDois.getPontuacao() + 1);
					informarVencedorRodada(jogadorDois.getNome());
				}
				break;
			case "spock":
				if(jogadorDois.getEscolha() == "tesoura" || jogadorDois.getEscolha() == "pedra"){
					jogadorUm.setPontuacao(jogadorUm.getPontuacao() + 1);
					informarVencedorRodada(jogadorUm.getNome());
				}else{
					jogadorDois.setPontuacao(jogadorDois.getPontuacao() + 1);
					informarVencedorRodada(jogadorDois.getNome());
				}
				break;
			case "pedra":
				if(jogadorDois.getEscolha() == "tesoura" || jogadorDois.getEscolha() == "lagarto"){
					jogadorUm.setPontuacao(jogadorUm.getPontuacao() + 1);
					informarVencedorRodada(jogadorUm.getNome());
				}else{
					jogadorDois.setPontuacao(jogadorDois.getPontuacao() + 1);
					informarVencedorRodada(jogadorDois.getNome());
				}
				break;
			case "lagarto":
				if(jogadorDois.getEscolha() == "papel" || jogadorDois.getEscolha() == "spock"){
					jogadorUm.setPontuacao(jogadorUm.getPontuacao() + 1);
					informarVencedorRodada(jogadorUm.getNome());
				}else{
					jogadorDois.setPontuacao(jogadorDois.getPontuacao() + 1);
					informarVencedorRodada(jogadorDois.getNome());
				}
				break;
			case "papel":
				if(jogadorDois.getEscolha() == "pedra" || jogadorDois.getEscolha() == "spock"){
					jogadorUm.setPontuacao(jogadorUm.getPontuacao() + 1);
					informarVencedorRodada(jogadorUm.getNome());
				}else{
					jogadorDois.setPontuacao(jogadorDois.getPontuacao() + 1);
					informarVencedorRodada(jogadorDois.getNome());
				}
				break;
			}
		}
		jogadorUm.setEscolha(null);
		jogadorDois.setEscolha(null);
	}
	
	private void informarVencedorRodada(String nomeVencedor){
		try {
			enviarMensagem(jogadorUm.getSocket().getOutputStream(),
					jogadorDois.getSocket().getOutputStream(),
					String.format("\n%s venceu essa rodada!!\n\rPlacar: %s %d X %d %s\r\nIniciando rodada %d\n\n\r",
							nomeVencedor, jogadorUm.getNome().toUpperCase(), jogadorUm.getPontuacao(),
							jogadorDois.getPontuacao(), jogadorDois.getNome().toUpperCase(), rodada));
		} catch (IOException e) {
			System.out.println("Erro ao enviar mensagem!!");
		}
	}
	
	public void informarVencedorPartida(OutputStream saida){
		try {
			String vencedor = "";
			
			if (jogadorUm.getPontuacao() > jogadorDois.getPontuacao()){
				vencedor = jogadorUm.getNome();
			}
			else
				vencedor = jogadorDois.getNome();
			
			enviarMensagem(saida,
					String.format("\n\r%s VENCEU A PARTIDA!!\n\rPlacar final: %s %d X %d %s\r\nFim de jogo!\n\r"
							+ "Parabens!! %s\n\n\r",
							vencedor.toUpperCase(), jogadorUm.getNome().toUpperCase(), jogadorUm.getPontuacao(),
							jogadorDois.getPontuacao(), jogadorDois.getNome().toUpperCase(), vencedor.toUpperCase()));
		} catch (Exception e) {
			System.out.println("Erro ao enviar mensagem!!");
		}
		finalizarJogo();
	}
	
	public void finalizarJogo(){
		try {
			System.out.println("Finalizando partida!\n\r");
			Thread.sleep(1000);
			this.numeroConexoes = 0;
			jogadorUm.getSocket().close();
			jogadorDois.getSocket().close();
			System.out.println("Servidor pronto para outro jogo!");
		} catch (Exception e) {
			System.out.println("Erro ao finalizar jogo!!");
		}
	}
	
	public void informarOpcoes(OutputStream saida){
		String opcoes = "\r\nRODADA " + rodada +"!!\r\n\r\n"
				+ "1-Pedra "
				+ "2-Papel "
				+ "3-Tesoura "
				+ "4-Lagarto "
				+ "5-Spock\r\n\r\n";
		enviarMensagem(saida, 
				opcoes);
	}
	
	public void enviarMensagem(OutputStream saida, String mensagem){
		try {
			saida.write(mensagem.getBytes());
		} catch (IOException e) {
			System.out.println("Erro ao enviar mensagem!!!!");
		}
	}
	
	public void enviarMensagem(OutputStream saida1, OutputStream saida2, String mensagem){
		try {
			saida1.write(mensagem.getBytes());
			saida2.write(mensagem.getBytes());
		} catch (IOException e) {
			System.out.println("Erro ao enviar mensagem!!");
		}
	}

	public static void main(String[] args) {
		try{
			Servidor servidor = new Servidor();
			if(args.length == 1){
				servidor.iniciarServidor(Integer.parseInt(args[0]));
			} else if(args.length == 0){
				servidor.iniciarServidor(1234);
			}else{
				throw new Exception("Argumento Invalido!");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
