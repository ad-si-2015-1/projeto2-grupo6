import java.net.Socket;


public class Jogador {
	private Socket socket;
	private String escolha;
	private String nome;
	private Integer pontuacao;
	
	public Jogador(Socket socket){
		this.socket = socket;
		pontuacao = 0;
		escolha = null;
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public String getEscolha() {
		return escolha;
	}

	public void setEscolha(String escolha) {
		this.escolha = escolha;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getPontuacao() {
		return pontuacao;
	}

	public void setPontuacao(Integer pontuacao) {
		this.pontuacao = pontuacao;
	}
	
}
