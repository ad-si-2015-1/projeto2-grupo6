import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;

public class ThreadJogo extends Thread {
	private Jogador jogador;
	private Servidor servidor;

	public ThreadJogo(Jogador jogador, Servidor servidor) {
		super();
		this.jogador = jogador;
		this.servidor = servidor;
	}

	public boolean ValidaResposta(String resposta) {
		if (resposta.equals("1") ||resposta.equals("2")
				|| resposta.equals("3")|| resposta.equals("5")
				|| resposta.equals("4")){
			return true;
		} else
			return false;
	}

	public void run() {
		try {

			InputStream entrada = null ;
			OutputStream saida = null ;
			Scanner reader = null;

			try {
			     entrada = jogador.getSocket().getInputStream();
				 saida = jogador.getSocket().getOutputStream();
				 reader = new Scanner(entrada);
				
			} catch (IOException e) {
				System.out.println("Erro ao obter inputStream");
			}
			
			servidor.enviarMensagem(saida, "Diga seu nome: ");	            
			jogador.setNome(reader.nextLine());
				
			String resposta;
			while (servidor.getJogadorUm().getPontuacao() != 3 && servidor.getJogadorDois().getPontuacao() != 3) {
				Thread.sleep(750);
				if(jogador.getEscolha() == null && (servidor.getJogadorUm().getPontuacao() != 3 
						&& servidor.getJogadorDois().getPontuacao() != 3)){
					reader.reset();
					servidor.informarOpcoes(saida);
	                servidor.enviarMensagem(saida, String.format("%s digite o numero da opcao escolhida:", jogador.getNome()));
	                resposta = reader.nextLine();
	                while(!ValidaResposta(resposta)){
	                	servidor.enviarMensagem(saida, String.format("Opcao invalida %s digite novamente:", jogador.getNome()));
	                	reader.reset();
	                	resposta = reader.nextLine();
	                }
	                int escolha = Integer.parseInt(resposta);
	                servidor.setEscolha(escolha, jogador);
	                if(servidor.getJogadorUm().getEscolha() == null || servidor.getJogadorDois().getEscolha() == null){
	                	servidor.enviarMensagem(saida, "Aguarde...\n\r");
	                }
				}
			}
			servidor.informarVencedorPartida(saida);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
