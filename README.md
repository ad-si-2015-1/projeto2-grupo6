# Projeto do jogo "Pedra-papel-tesoura-lagarto-Spock" utilizando sockets

## Resumo :

### Cada jogador escolher entre pedra, papel, tesoura, lagarto ou Spock.
### Enviam a escolha para o servidor que computa um ponto para o vencedor
### da rodada.

## Quantidade de jodadores:

### 2 jogadores

## Regras:

### Tesoura corta papel
### Papel cobre pedra
### Pedra esmaga lagarto
### Lagarto envenena Spock
### Spock esmaga (ou derrete) tesoura
### Tesoura decapita lagarto
### Lagarto come papel
### Papel refuta Spock
### Spock vaporiza pedra
### Pedra quebra tesoura

## Como vencer o jogo:

### Ganhador da melhor de 5. (Jogador que fizer 3 pontos primeiro).

## Referências do jogo:

### Criado por Sam Kass e Karen Bryla, e referenciado em The Big Bang Theory.

