O jogo é uma especie de pedra-papel-tesoura incrementado.
Ganhou notoriedade atravez da serie The Big Bang Theory, da Warner.
O jogo funciona com dois jogadores acessando um server, que serve de mediador.
O jogo e jogado em melhor de 5.

Segue o seguinte fluxo:
player 1 envia requisicao;
server pergunta o nome;
player 1 responde o nome;
server responde "aguarde player dois";
caso passem 60 segundos sem outro jogador logar, o server envia mensagem de
timeout para o player 1;

player 2 envia requisicao;
server pergunta o nome;
player 2 responde o nome;

server responde enviando escolhas aos DOIS jogadores;

terceiro jogador envia requisicao;
server responde: Estamos cheios. Tente novamente mais tarde.;
n jogador(...) Estamos cheios. Tente novamente mais tarde.;

Ou seja, enquanto dois estão jogando, o server responde com mensagem "Estamos cheios. Tente novamente mais tarde" para
qualquer um que tente acessar.

players enviam opções;
server recebe opções, processa, e envia resultado para ambos;

Caso opção invalida, server responde "opção invalida", e pergunta novamente;

exemplo de resposta do servidor:

"Guthi escolheu spock
Valéria excolheu lagarto
Guthi venceu

Placar: 0x1"

E solicita novamente que os players escolham as opcoes.

players enviam opções;
server recebe opções, processa, e envia resultado para ambos;

Repete isso até um jogador atingir 3 pontos, então é fim de jogo, server dispensa os dois jogadores e
aguarda novos jogadores.
